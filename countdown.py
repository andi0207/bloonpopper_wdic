from random import seed	#Benötigte Librarys
from random import randint
from time import sleep
import time

time_start = time.perf_counter()	#speichert die Zeit zum Start des Programms ab

x = randint(0, 10)			#Zufallszahl von 0 bis 10
print(x)					#Ausgabe der Zufallszahl
time_start = time.perf_counter()	#speichert die Zeit zum Start des Countdowns ab
for i in range(x):				#Schleife, um die Zeit der Zufallszahl in sec zu warten
    sleep(1)

time_stop = time.perf_counter()		#speichert die Zeit nach Ablauf des Countdowns
countdown_time = time_stop - time_start	#Berechner der Zeit für einen Durchlauf des Countdowns
print(f"Countdown Time: {countdown_time}")	#Aufgabe der Berechneten Zeit
